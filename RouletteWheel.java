import java.util.*;
public class RouletteWheel {
    private Random r;
    private int number = 0;
    public RouletteWheel() {
        this.r = new Random();
    }
    public void spin() {
        number = r.nextInt(37);
    }
    public int getValue() {
        return this.number;
    }
}
