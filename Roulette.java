import java.util.*;
public class Roulette {
    public static void main(String[] args) {
        RouletteWheel wheel = new RouletteWheel();
        Scanner reader = new Scanner(System.in);
        int money = 10;
        int menuChoice = 0;
        Random r = new Random();
        while(menuChoice != -1) {
            System.out.println("Money: $" + money);
            System.out.println("What would you like to do?");
            System.out.println("1. Make a bet");
            System.out.println("-1. Quit");
            menuChoice = reader.nextInt();
            if(menuChoice == 1) {
                System.out.println("How much would you like to bet?");
                int betAmount = reader.nextInt();
                if(betAmount > money) {
                    System.out.println("Invalid amount.");
                }
                else {
                    System.out.println("What would you like to bet on?");
                    System.out.println("1. Specific Number");
                    System.out.println("2. Color");
                    System.out.println("3. Even or Odd");
                    menuChoice = reader.nextInt();
                    switch(menuChoice) {
                        case 1:
                            System.out.println("Which number will you bet on?");
                            menuChoice = reader.nextInt();
                            if(betAmount > money) {
                                System.out.println("Invalid amount.");
                            }
                            else {
                                wheel.spin();
                                int number = wheel.getValue();
                                System.out.println("The ball stopped at " + number);
                                money -= betAmount;
                                if(menuChoice == number) {
                                    money += betAmount*35;
                                    System.out.println("You win!");
                                }
                                else {
                                    System.out.println("You lose!");
                                }
                            }
                            break;
                        case 2:
                            System.out.println("Which color will you bet on?");
                            System.out.println("0. Red");
                            System.out.println("1. Black");
                            menuChoice = reader.nextInt();
                            int colorSpin = r.nextInt(2);
                            if(menuChoice == colorSpin) {
                                money += betAmount;
                            }
                            else {
                                System.out.println("You lose!");
                            }
                            break;
                        case 3:
                            System.out.println("Will you bet on Even or Odd numbers?");
                            break;
                    }
                }
            }
        }
        System.out.println("Goodbye, and remember: The House Always Wins.");
    }
}